import { ReceiptComponent } from './components/receipt/receipt.component';
import { OrderComponent } from './components/order/order.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { orderReducer } from './reducers/order.reducer';
import { MenuComponent } from './components/menu/menu.component';
import { ImageTileComponent } from './components/image-tile/image-tile.component';
import { CurrentSalesComponent } from './components/current-sales/current-sales.component';
import { RouterModule } from '@angular/router';
import { OrderEffects } from './effects/order.effect';



@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ImageTileComponent,
    CurrentSalesComponent,
    ReceiptComponent,
    OrderComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    StoreModule.forRoot({
      order: orderReducer
    }),
    EffectsModule.forRoot([
      OrderEffects
    ]),
    StoreDevtoolsModule.instrument({
      maxAge: 50
    }),
    RouterModule.forRoot(
      [
        { path: 'order', component: OrderComponent },
        { path: 'receipt', component: ReceiptComponent },
        { path: '',  redirectTo: '/order', pathMatch: 'full' }
      ]
    )
    // other imports here
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
