export interface OrderResponse {
  event: string;
  amount: number;
}
