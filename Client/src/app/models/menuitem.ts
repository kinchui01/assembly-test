export interface MenuItem {
  name: string;
  price: number;
  src: string;
}
