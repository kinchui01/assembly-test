import { Order } from "./order";

export interface AppState {
  order: Order;
}
