import { OrderResponse } from './order-response';
import { MenuItem } from './menuitem';

export interface Order {
  orderlist: MenuItem[];
  orderResponse: OrderResponse;
}
