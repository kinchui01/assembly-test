import { Router } from '@angular/router';
import { OrderResponse } from './../models/order-response';
import { Store } from '@ngrx/store';
import { switchMap, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import * as orderActions from './../actions/order.actions';
import { Observable } from 'rxjs';
import { AppState } from '../models/app-state';

@Injectable()
export class OrderEffects {
  constructor(
    private actions: Actions,
    private store: Store<AppState>,
    private router: Router
  ) {}

  @Effect()
  loadOrderDetails = this.actions.pipe(
    ofType<orderActions.UpdateOrderHttpAction>(
      orderActions.UPDATE_ORDER_HTTP_ACTION
    ),
    switchMap(action => ResponseObservable(action)),
    map((res: OrderResponse) => {
      if (res) {
        this.router.navigateByUrl('/receipt');
        return new orderActions.LoadOrderHttpSuccessAction(res);
      }
    })
  );
}

const ResponseObservable = action => {
  return Observable.create(observer => {
    const total = action.order.reduce((sum, item) => {
      return sum + item.price;
    }, 0);

    const connection = new WebSocket('ws://demos.kaazing.com/echo');

    connection.onopen = function() {
      connection.send(`{"event": "purchase", "amount": ${total}}`);
    };

    connection.onerror = function(err) {
      console.error('Error: ' + err);
      observer.error();
    };

    // Log messages from the server
    connection.onmessage = function(e) {
      connection.close();
      observer.next(JSON.parse(e.data));
      observer.complete();
    };
  });
};
