import { OrderResponse } from './../../models/order-response';
import { MenuItem } from './../../models/menuitem';
import { AppState } from './../../models/app-state';
import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Router } from '@angular/router';

import * as orderAction from './../../actions/order.actions';

import { take } from 'rxjs/operators';

@Component({
  selector: 'receipt',
  templateUrl: './receipt.component.html',
  styleUrls: ['./receipt.component.css']
})
export class ReceiptComponent implements OnInit {
  constructor(private store: Store<AppState>, private router: Router) {}

  orderList: MenuItem[] = null;
  orderResponse: OrderResponse = null;
  hasResponse = false;

  ngOnInit() {
    this.store
      .pipe(
        select((store: AppState) => store.order),
        take(1)
      )
      .subscribe(data => {
        if (data.orderResponse && data.orderResponse.event === 'purchase') {
          this.orderResponse = data.orderResponse;
          this.orderList = data.orderlist;
          this.hasResponse = true;
          this.store.dispatch(new orderAction.ClearOrderAction());
        } else {
          console.error('No order response');
          this.router.navigateByUrl('/order');
        }
      });
  }
}
