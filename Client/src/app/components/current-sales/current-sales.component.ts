import { MenuItem } from './../../models/menuitem';
import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { AppState } from 'src/app/models/app-state';

import * as orderAction from './../../actions/order.actions';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'current-sales',
  templateUrl: './current-sales.component.html',
  styleUrls: ['./current-sales.component.css']
})
export class CurrentSalesComponent implements OnInit {
  constructor(
    private store: Store<AppState>,
    private router: Router,
    private http: HttpClient
  ) {}
  orderList: MenuItem[] = null;
  total = 0;
  numberOfItems = 0;
  isError = false;
  onSubmit = false;
  isLoading = false;

  ngOnInit() {
    this.store
      .select((state: AppState) => state.order)
      .subscribe((order: any) => {
        if (order.orderlist) {
          this.orderList = order.orderlist;
          this.total = 0;
          this.numberOfItems = this.orderList.length;
          this.onSubmit = false;
          this.isError = false;
          this.orderList.map(item => {
            this.total += item.price;
          });
        }
      });
  }

  deleteOrder(e: Event, order: MenuItem) {
    this.store.dispatch(new orderAction.DeleteOrderItemAction(order));
  }

  submitOrder() {
    this.onSubmit = true;
    if (this.numberOfItems <= 0) {
      this.isError = true;
      return;
    } else {
      this.isLoading = true;

      this.store.dispatch(
        new orderAction.UpdateOrderHttpAction(this.orderList)
      );
    }
  }
}
