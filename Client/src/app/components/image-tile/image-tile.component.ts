
import { Store } from '@ngrx/store';
import { Component, OnInit, Input } from '@angular/core';
import { MenuItem } from 'src/app/models/menuitem';
import { AppState } from 'src/app/models/app-state';

import * as orderAction from './../../actions/order.actions';

@Component({
  selector: 'image-tile',
  templateUrl: './image-tile.component.html',
  styleUrls: ['./image-tile.component.css']
})
export class ImageTileComponent implements OnInit {
  constructor(private store: Store<AppState>) {}

  @Input()
  item: MenuItem = null;

  ngOnInit() {}

  UpdateSales(e: Event, item: MenuItem) {
    this.store.dispatch(new orderAction.UpdateOrderDataAction(item));
  }
}
