import { MenuItem } from './../../models/menuitem';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  constructor() {}

  menuList: MenuItem[] = null;

  ngOnInit() {
    this.menuList = [
      {
        name: 'Hamburger',
        price: 6.95,
        src: 'assets/images/hamburger.jpg'
      },
      {
        name: 'Chicken Wings',
        price: 4.95,
        src: 'assets/images/chicken-wings.jpg'
      },
      {
        name: 'Chicken Nuggets',
        price: 5.95,
        src: 'assets/images/chicken-nuggets.jpg'
      },
      {
        name: 'Hot Chips',
        price: 3.95,
        src: 'assets/images/hot-chips.jpg'
      },
      {
        name: 'Coke (can)',
        price: 2.5,
        src: 'assets/images/coke-can.jpg'
      },
      {
        name: 'Coke (bottle)',
        price: 3.0,
        src: 'assets/images/coke-bottle.jpg'
      },
      {
        name: 'Milkshake',
        price: 6.95,
        src: 'assets/images/milkshake.jpg'
      },
      {
        name: 'Ice Cream',
        price: 6.95,
        src: 'assets/images/ice-cream.jpg'
      }
    ];
  }
}
