import * as orderActions from '../actions/order.actions';

const initState = { orderlist: [], orderResponse: null };
export function orderReducer(state = initState, action: orderActions.orderAction) {
  switch (action.type) {
    case orderActions.UPDATE_ORDER_ACTION:
      state.orderlist.push(action.item);
      return state;

    case orderActions.DELETE_ORDER_ITEM_ACTION:
      let isRemoved = false;
      state.orderlist = state.orderlist.filter(x => {
        if (x.name === action.orderItem.name && !isRemoved) {
          isRemoved = true;
          return false;
        }
        return true;
      });
      return Object.assign({}, state);

    case orderActions.LOAD_ORDER_HTTP_SUCCESS_ACTION:
      state.orderResponse = action.payload;
      return state;

    case orderActions.CLEAR_ORDER_ACTION:
      state.orderlist = [];
      state.orderResponse = null;
      return state;

    default:
      return state;
  }
}
