import { OrderResponse } from './../models/order-response';
import { MenuItem } from '../models/menuitem';

export const LOAD_ORDER_ACTION = 'LOAD_ORDER_ACTION';
export const UPDATE_ORDER_ACTION = 'UPDATE_ORDER_ACTION';
export const DELETE_ORDER_ITEM_ACTION = 'DELETE_ORDER_ITEM_ACTION';
export const UPDATE_ORDER_HTTP_ACTION = 'UPDATE_ORDER_HTTP_ACTION';
export const LOAD_ORDER_HTTP_SUCCESS_ACTION = 'LOAD_ORDER_HTTP_SUCCESS_ACTION';
export const CLEAR_ORDER_ACTION = 'CLEAR_ORDER_ACTION';

export class LoadOrderDataAction {
  readonly type = LOAD_ORDER_ACTION;
  constructor(public payload: any) {}
}

export class UpdateOrderDataAction {
  readonly type = UPDATE_ORDER_ACTION;
  constructor(public item: MenuItem) {}
}

export class DeleteOrderItemAction {
  readonly type = DELETE_ORDER_ITEM_ACTION;
  constructor(public orderItem: MenuItem) {}
}

export class UpdateOrderHttpAction {
  readonly type = UPDATE_ORDER_HTTP_ACTION;
  constructor(public order: any) {}
}

export class LoadOrderHttpSuccessAction {
  readonly type = LOAD_ORDER_HTTP_SUCCESS_ACTION;
  constructor(public payload: OrderResponse) {}
}

export class ClearOrderAction {
  readonly type = CLEAR_ORDER_ACTION;
  constructor() {}
}


export type orderAction =
  | LoadOrderDataAction
  | UpdateOrderDataAction
  | DeleteOrderItemAction
  | UpdateOrderHttpAction
  | LoadOrderHttpSuccessAction
  | ClearOrderAction;
